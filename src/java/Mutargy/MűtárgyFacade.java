/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mutargy;

import Entity.Műtárgy;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author András
 */
@Stateless
public class MűtárgyFacade extends AbstractFacade<Műtárgy> {

    @PersistenceContext(unitName = "KettesCsapatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MűtárgyFacade() {
        super(Műtárgy.class);
    }
    
}
