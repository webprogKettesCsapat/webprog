package Mutargy;

import Controller.LoginController;
import Entity.Műtárgy;
import controllers.FelhasználóFacade;
import controllers.KiállításFacade;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "műtárgyController")
@ViewScoped
public class MűtárgyController extends AbstractController<Műtárgy> {

    List<Műtárgy> lista = new ArrayList<>(); 
    String kiallitasnev;
    int ar;
    int id;
    List<Műtárgy> list;
    
    @EJB
    KiállításFacade facade;
    
    @EJB
    FelhasználóFacade felhasználóFacade;
    
    @Inject
    private LoginController loginController;
    
    public MűtárgyController() {
        // Inform the Abstract parent controller of the concrete Műtárgy Entity
        super(Műtárgy.class);
    }

    public LoginController getLoginController() {
        return loginController;
    }

    public void setLoginController(LoginController loginController) {
        this.loginController = loginController;
    }

    
    
    /**
     * Sets the "items" attribute with a collection of Kiállítás entities that
     * are retrieved from Műtárgy?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Kiállítás page
     */
    public String navigateKiállításCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Kiállítás_items", this.getSelected().getKiállításCollection());
        }
        return "/kiállítás/index";
    }

    public String getKiallitasnev() {
        return kiallitasnev;
    }

    public void setKiallitasnev(String kiallitasnev) {
        this.kiallitasnev = kiallitasnev;
    }

    public int getAr() {
        return ar;
    }

    public void setAr(int ar) {
        this.ar = ar;
    }

    /**
     * Sets the "items" attribute with a collection of Rendelés entities that
     * are retrieved from Műtárgy?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Rendelés page
     */
    public String navigateRendelésCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Rendelés_items", this.getSelected().getRendelésCollection());
        }
        return "/rendelés/index";
    }

    public void add(Műtárgy m){
        if(!lista.contains(m)){
            lista.add(m);            
        }
        System.out.println(lista);
    }
    
    public void delete(){
        lista.clear();
        System.out.println(lista);
    }

    public List<Műtárgy> getList() {
        System.out.println("Getlist megy " + getId());
        if(list == null){
            list = new ArrayList<>();
            list.addAll(facade.getkiall(getId()).getMűtárgyCollection());
            System.out.println(list);
        }
        return list;
    }

    public void setList(List<Műtárgy> list) {
        this.list = list;
    }
    
    public Integer getId() {
        Map<String, String> paramMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (paramMap.containsKey("id")) {
            id = Integer.valueOf(paramMap.get("id"));
        }
        return id;
    }
    
    public void saveKiallitas(){
        if("".equals(kiallitasnev) || lista.isEmpty()){
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Kérem adja meg a kiállítás nevét és az elemeit!") );
        }else{
            facade.saveKiall(ar, kiallitasnev, lista, felhasználóFacade.getByUsername(loginController.getUsername()));
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Új kiállítást hozott létre!") );
        }
        ar=0;
        kiallitasnev = "";
        lista.clear();
    }
}
