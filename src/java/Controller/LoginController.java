/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import database.SessionUtils;
import database.LoginDAO;
import java.awt.Event;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

/**
 *
 * @author czakkos
 */
@ManagedBean
@SessionScoped
@Named(value = "loginController")
public class LoginController implements Serializable{
    private static final long serialVersionUID = 1094801825228386363L;
    private String username;
    private String password;
    private boolean isLoggedIn = false;   
    

    public boolean isIsLoggedIn() {
        return isLoggedIn;
    }

    public void setIsLoggedIn(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }
    
    

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    //validate login
    public String validateUsernamePassword() {
            boolean valid = LoginDAO.validate(username, password);
            if (valid) {
                    HttpSession session = SessionUtils.getSession();
                    session.setAttribute("username", username);
                    isLoggedIn = true;
                    return "admin";
            } else {
                FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_WARN,
                                "Incorrect Username and Passowrd",
                                "Please enter correct username and Password"));
                isLoggedIn = false;
                return "login";
            }
    }
    
    public String getRole(){
        System.out.println("Role: "+LoginDAO.getRole(username, password));
        String s = LoginDAO.getRole(username, password);
        if("1".equals(s) || "2".equals(s) || "3".equals(s)) {
            return "1";
        }
        return "0";
    }
    

    //logout event, invalidate session
    public String logout() throws IOException {
        //FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        //isLoggedIn = false;
        //ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        //ec.invalidateSession();
        return "/index";
    }
}
