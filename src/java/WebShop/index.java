/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebShop;

import Entity.Termekek;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 *
 * @author gabor_000
 */
@ManagedBean
@Named(value = "index")
@RequestScoped
public class index implements Serializable{

    @EJB
    private TermekFacade termekFacade;
    
    public index() {
    }
    
    public List<Termekek> getTermekek(){
        return termekFacade.retrieveTermekek();
    }
    
    
    public String goToPage() {
        return "/webshop/order.xhtml?faces-redirect=true";
    }
    
    public String goToHomePage() {
        return "/webshop/index.xhtml?faces-redirect=true";
    }
}
