package WebShop;

import Entity.Termekek;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;

@Stateless
public class TermekFacade {

    @PersistenceContext(unitName = "KettesCsapatPU")
    private EntityManager em;
    private EntityTransaction et;
    
    public List<Termekek> retrieveTermekek(){
        return em.createQuery("SELECT p FROM Termekek p").getResultList();
    }

    public int checkIfQueryExists(String query){
        List<Termekek> termekek = em.createQuery("SELECT p FROM Termekek p WHERE p.termekID = :termekID").setParameter("termekID", query).getResultList();
        return termekek.size();
    }
    
    public Termekek returnTermek(String query){
        Termekek termek = (Termekek)em.createQuery("SELECT p FROM p WHERE p.termekID = :termekID").setParameter("termekID", query).getSingleResult();
        return termek;
    }

}
