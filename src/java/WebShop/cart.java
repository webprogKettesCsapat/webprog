/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebShop;

import Entity.Termekek;
import javax.inject.Named;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author gabor_000
 */
@Named(value = "cart")
@ManagedBean
@SessionScoped
public class cart implements Serializable {

    private List<Termekek> termekek = new ArrayList();
    
    public cart() {
    }
    
    public void add(Termekek termek){
        termekek.add(termek);
    }
    
    public void remove(Termekek termek){
        termekek.remove(termek);
    }
    
    public int getCartCount(){
        return termekek.size();
    }
    
    
    public int getCartSum(){
        int sum = 0;
        for(Termekek obj: termekek){
            sum+=obj.getAr();
        }
        return sum;
    }
    
    public Map<Termekek, Integer> getCartContent(){
        Map<Termekek, Integer> cartContents = new HashMap<>();
        for(Termekek obj: termekek){
            if(cartContents.containsKey(obj)){
                cartContents.put(obj, cartContents.get(obj)+1);
            }else{
                cartContents.put(obj, 1);
            }
        }
        return cartContents;
    }
    
}
