/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebShop;

import Entity.Termekek;
import java.io.IOException;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;


/**
 *
 * @author gabor_000
 */

@ManagedBean

@RequestScoped
public class product implements Serializable{

    @EJB
    private TermekFacade termekFacade;
    
    cart myCart;
    /**
     * Creates a new instance of product
     */
    
    public product() {
    }
    
    public String getQuery(){
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("query");
    }
    
    public Termekek getTermek(){
        return termekFacade.returnTermek(getQuery());
    }
    
    public void addToCart(){
        String query = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("query");
        myCart.add(termekFacade.returnTermek(query));
    }
    
}
