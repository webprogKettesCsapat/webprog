/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Entity.Felhasználóicsoport;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author András
 */
@Stateless
public class FelhasználóicsoportFacade extends AbstractFacade<Felhasználóicsoport> {

    @PersistenceContext(unitName = "KettesCsapatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FelhasználóicsoportFacade() {
        super(Felhasználóicsoport.class);
    }
    
}
