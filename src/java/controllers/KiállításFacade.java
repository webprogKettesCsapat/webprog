/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Entity.Felhasználó;
import Entity.Kiállítás;
import Entity.Műtárgy;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author András
 */
@Stateless
public class KiállításFacade extends AbstractFacade<Kiállítás> {

    @PersistenceContext(unitName = "KettesCsapatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public KiállításFacade() {
        super(Kiállítás.class);
    }
    public Kiállítás getkiall(int id){
        System.out.println("Elindul");
        List<Kiállítás> kilist = getEntityManager().createNamedQuery("Ki\u00e1ll\u00edt\u00e1s.findById")
                .setParameter("id", id)
                .getResultList();
        if(kilist!=null){
            return kilist.get(0);
        }
        return null;
    }
    
    public void saveKiall(int ticket, String title, List<Műtárgy> list, Felhasználó felhas){
        Kiállítás kiall = new Kiállítás();
        kiall.setJegyár(ticket);
        kiall.setKurátor(felhas);
        kiall.setNeve(title);
        kiall.setMűtárgyCollection(list);
        em.persist(kiall);
    }
    
    public void saveTicket(Felhasználó felhasználó, Kiállítás kiall){
        List<Kiállítás> list = new ArrayList<>();
        list.addAll(felhasználó.getKiállításCollection());
        if(felhasználó.getEgyenleg() >= kiall.getJegyár()){
            if(!list.contains(kiall)){
                list.add(kiall);
                felhasználó.setKiállításCollection(list);
                felhasználó.setEgyenleg(felhasználó.getEgyenleg()-kiall.getJegyár());
                em.merge(felhasználó);
            }else{
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Már megvette ezt a tárlatot!") );
            }
        }else{
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Nincs elég egyenlege!") );
        }
    }
}
