/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Controller.LoginController;
import Entity.Felhasználó;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author András
 */
@Stateless
public class FelhasználóFacade extends AbstractFacade<Felhasználó> {
    
    @PersistenceContext(unitName = "KettesCsapatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FelhasználóFacade() {
        super(Felhasználó.class);
    }
    
   
    public Felhasználó getByUsername(String name){
        List<Felhasználó> f = getEntityManager().createNamedQuery("Felhaszn\u00e1l\u00f3.findByFelhaszn\u00e1l\u00f3n\u00e9v")
                .setParameter("felhaszn\u00e1l\u00f3n\u00e9v", name)
                .getResultList();
        if(f != null){
            return f.get(0);
        }
        return null;
    }
    
    public void addPenz(int osszeg, Felhasználó felhasználó){
        felhasználó.setEgyenleg(felhasználó.getEgyenleg() + osszeg);
        em.merge(felhasználó);
    }
}
