package bean;

import Entity.Felhasználó;
import controllers.FelhasználóFacade;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "felhasználóController")
@ViewScoped
public class FelhasználóController extends AbstractController<Felhasználó> {

    @Inject
    private FelhasználóicsoportController felhasználóicsoportIDController;

    @EJB
    FelhasználóFacade facade;
    
    int osszeg = 0;
    
    public FelhasználóController() {
        // Inform the Abstract parent controller of the concrete Felhasználó Entity
        super(Felhasználó.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        felhasználóicsoportIDController.setSelected(null);
    }

    /**
     * Sets the "items" attribute with a collection of Kiállítás entities that
     * are retrieved from Felhasználó?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for Kiállítás page
     */
    public String navigateKiállításCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Kiállítás_items", this.getSelected().getKiállításCollection());
        }
        return "/kiállítás/index";
    }

    /**
     * Sets the "items" attribute with a collection of Kiállítás entities that
     * are retrieved from Felhasználó?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for Kiállítás page
     */
    public String navigateKiállításCollection1() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Kiállítás_items", this.getSelected().getKiállításCollection1());
        }
        return "/kiállítás/index";
    }

    /**
     * Sets the "items" attribute with a collection of Rendelés entities that
     * are retrieved from Felhasználó?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for Rendelés page
     */
    public String navigateRendelésCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Rendelés_items", this.getSelected().getRendelésCollection());
        }
        return "/rendelés/index";
    }

    /**
     * Sets the "selected" attribute of the Felhasználóicsoport controller in
     * order to display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareFelhasználóicsoportID(ActionEvent event) {
        if (this.getSelected() != null && felhasználóicsoportIDController.getSelected() == null) {
            felhasználóicsoportIDController.setSelected(this.getSelected().getFelhasználóicsoportID());
        }
    }

    public int getOsszeg() {
        return osszeg;
    }

    public void setOsszeg(int osszeg) {
        this.osszeg = osszeg;
    }
    
    public void add(Felhasználó felhasználó){
        facade.addPenz(osszeg, felhasználó);
        osszeg = 0;
    }
}
