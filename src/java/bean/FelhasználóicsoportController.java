package bean;

import Entity.Felhasználóicsoport;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "felhasználóicsoportController")
@ViewScoped
public class FelhasználóicsoportController extends AbstractController<Felhasználóicsoport> {

    public FelhasználóicsoportController() {
        // Inform the Abstract parent controller of the concrete Felhasználóicsoport Entity
        super(Felhasználóicsoport.class);
    }

    /**
     * Sets the "items" attribute with a collection of Felhasználó entities that
     * are retrieved from Felhasználóicsoport?cap_first and returns the
     * navigation outcome.
     *
     * @return navigation outcome for Felhasználó page
     */
    public String navigateFelhasználóCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Felhasználó_items", this.getSelected().getFelhasználóCollection());
        }
        return "/felhasználó/index";
    }

}
