package bean;

import Controller.LoginController;
import Entity.Felhasználó;
import Entity.Kiállítás;
import controllers.FelhasználóFacade;
import controllers.KiállításFacade;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "kiállításController")
@ViewScoped
public class KiállításController extends AbstractController<Kiállítás> {

    List<Kiállítás> lista;
    
    @Inject
    private FelhasználóController kurátorController;

    @EJB
    KiállításFacade facade;
    
    @EJB
    FelhasználóFacade felhasználóFacade;
    
    @Inject
    private LoginController loginController;
    
    public KiállításController() {
        // Inform the Abstract parent controller of the concrete Kiállítás Entity
        super(Kiállítás.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        kurátorController.setSelected(null);
    }

    /**
     * Sets the "items" attribute with a collection of Felhasználó entities that
     * are retrieved from Kiállítás?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for Felhasználó page
     */
    public String navigateFelhasználóCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Felhasználó_items", this.getSelected().getFelhasználóCollection());
        }
        return "/felhasználó/index";
    }

    /**
     * Sets the "items" attribute with a collection of Műtárgy entities that are
     * retrieved from Kiállítás?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Műtárgy page
     */
    public String navigateMűtárgyCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Műtárgy_items", this.getSelected().getMűtárgyCollection());
        }
        return "/műtárgy/index";
    }

    /**
     * Sets the "selected" attribute of the Felhasználó controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareKurátor(ActionEvent event) {
        if (this.getSelected() != null && kurátorController.getSelected() == null) {
            kurátorController.setSelected(this.getSelected().getKurátor());
        }
    }
    
    public void sellTicket(Kiállítás ki){
        
    }

    public List<Kiállítás> getLista() {
        if(lista == null){
            lista = new ArrayList<>();
            lista.addAll(felhasználóFacade.getByUsername(loginController.getUsername()).getKiállításCollection());
        }
        return lista;
    }

    public void setLista(List<Kiállítás> lista) {
        this.lista = lista;
    }
    
    public void addTicket(Kiállítás kiall){
        Felhasználó felh = felhasználóFacade.getByUsername(loginController.getUsername());
        facade.saveTicket(felh, kiall);
    }
}
