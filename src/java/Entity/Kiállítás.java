/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author András
 */
@Entity
@Table(name = "Ki\u00e1ll\u00edt\u00e1s")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ki\u00e1ll\u00edt\u00e1s.findAll", query = "SELECT k FROM Ki\u00e1ll\u00edt\u00e1s k")
    , @NamedQuery(name = "Ki\u00e1ll\u00edt\u00e1s.findById", query = "SELECT k FROM Ki\u00e1ll\u00edt\u00e1s k WHERE k.id = :id")
    , @NamedQuery(name = "Ki\u00e1ll\u00edt\u00e1s.findByNeve", query = "SELECT k FROM Ki\u00e1ll\u00edt\u00e1s k WHERE k.neve = :neve")
    , @NamedQuery(name = "Ki\u00e1ll\u00edt\u00e1s.findByJegy\u00e1r", query = "SELECT k FROM Ki\u00e1ll\u00edt\u00e1s k WHERE k.jegy\u00e1r = :jegy\u00e1r")})
public class Kiállítás implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 30)
    @Column(name = "Neve")
    private String neve;
    @Column(name = "Jegy\u00e1r")
    private Integer jegyár;
    @ManyToMany(mappedBy = "ki\u00e1ll\u00edt\u00e1sCollection")
    private Collection<Felhasználó> felhasználóCollection;
    @JoinTable(name = "Ki\u00e1ll\u00edt\u00e1s_elemei", joinColumns = {
        @JoinColumn(name = "Ki\u00e1ll\u00edt\u00e1s_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "M\u0171t\u00e1rgy_ID", referencedColumnName = "ID")})
    @ManyToMany
    private Collection<Műtárgy> műtárgyCollection;
    @JoinColumn(name = "Kur\u00e1tor", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Felhasználó kurátor;

    public Kiállítás() {
    }

    public Kiállítás(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNeve() {
        return neve;
    }

    public void setNeve(String neve) {
        this.neve = neve;
    }

    public Integer getJegyár() {
        return jegyár;
    }

    public void setJegyár(Integer jegyár) {
        this.jegyár = jegyár;
    }

    @XmlTransient
    public Collection<Felhasználó> getFelhasználóCollection() {
        return felhasználóCollection;
    }

    public void setFelhasználóCollection(Collection<Felhasználó> felhasználóCollection) {
        this.felhasználóCollection = felhasználóCollection;
    }

    @XmlTransient
    public Collection<Műtárgy> getMűtárgyCollection() {
        return műtárgyCollection;
    }

    public void setMűtárgyCollection(Collection<Műtárgy> műtárgyCollection) {
        this.műtárgyCollection = műtárgyCollection;
    }

    public Felhasználó getKurátor() {
        return kurátor;
    }

    public void setKurátor(Felhasználó kurátor) {
        this.kurátor = kurátor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kiállítás)) {
            return false;
        }
        Kiállítás other = (Kiállítás) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Ki\u00e1ll\u00edt\u00e1s[ id=" + id + " ]";
    }
    
}
