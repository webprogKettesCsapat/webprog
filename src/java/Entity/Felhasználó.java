/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author András
 */
@Entity
@Table(name = "Felhaszn\u00e1l\u00f3")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Felhaszn\u00e1l\u00f3.findAll", query = "SELECT f FROM Felhaszn\u00e1l\u00f3 f")
    , @NamedQuery(name = "Felhaszn\u00e1l\u00f3.findById", query = "SELECT f FROM Felhaszn\u00e1l\u00f3 f WHERE f.id = :id")
    , @NamedQuery(name = "Felhaszn\u00e1l\u00f3.findByMailc\u00edm", query = "SELECT f FROM Felhaszn\u00e1l\u00f3 f WHERE f.mailc\u00edm = :mailc\u00edm")
    , @NamedQuery(name = "Felhaszn\u00e1l\u00f3.findByFelhaszn\u00e1l\u00f3n\u00e9v", query = "SELECT f FROM Felhaszn\u00e1l\u00f3 f WHERE f.felhaszn\u00e1l\u00f3n\u00e9v = :felhaszn\u00e1l\u00f3n\u00e9v")
    , @NamedQuery(name = "Felhaszn\u00e1l\u00f3.findByJelsz\u00f3", query = "SELECT f FROM Felhaszn\u00e1l\u00f3 f WHERE f.jelsz\u00f3 = :jelsz\u00f3")})
public class Felhasználó implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "Egyenleg")
    private int egyenleg;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 30)
    @Column(name = "Mail_c\u00edm")
    private String mailcím;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "Felhaszn\u00e1l\u00f3n\u00e9v")
    private String felhasználónév;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "Jelsz\u00f3")
    private String jelszó;
    @JoinTable(name = "Jegy_elad\u00e1s", joinColumns = {
        @JoinColumn(name = "Felhaszn\u00e1l\u00f3_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "Ki\u00e1ll\u00edt\u00e1s_ID", referencedColumnName = "ID")})
    @ManyToMany
    private Collection<Kiállítás> kiállításCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kur\u00e1tor")
    private Collection<Kiállítás> kiállításCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "felhaszn\u00e1l\u00f3ID")
    private Collection<Rendelés> rendelésCollection;
    @JoinColumn(name = "Felhaszn\u00e1l\u00f3i_csoport_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Felhasználóicsoport felhasználóicsoportID;

    public Felhasználó() {
    }

    public Felhasználó(Integer id) {
        this.id = id;
    }

    public Felhasználó(Integer id, String felhasználónév, String jelszó) {
        this.id = id;
        this.felhasználónév = felhasználónév;
        this.jelszó = jelszó;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMailcím() {
        return mailcím;
    }

    public void setMailcím(String mailcím) {
        this.mailcím = mailcím;
    }

    public String getFelhasználónév() {
        return felhasználónév;
    }

    public void setFelhasználónév(String felhasználónév) {
        this.felhasználónév = felhasználónév;
    }

    public String getJelszó() {
        return jelszó;
    }

    public void setJelszó(String jelszó) {
        this.jelszó = jelszó;
    }

    @XmlTransient
    public Collection<Kiállítás> getKiállításCollection() {
        return kiállításCollection;
    }

    public void setKiállításCollection(Collection<Kiállítás> kiállításCollection) {
        this.kiállításCollection = kiállításCollection;
    }

    @XmlTransient
    public Collection<Kiállítás> getKiállításCollection1() {
        return kiállításCollection1;
    }

    public void setKiállításCollection1(Collection<Kiállítás> kiállításCollection1) {
        this.kiállításCollection1 = kiállításCollection1;
    }

    @XmlTransient
    public Collection<Rendelés> getRendelésCollection() {
        return rendelésCollection;
    }

    public void setRendelésCollection(Collection<Rendelés> rendelésCollection) {
        this.rendelésCollection = rendelésCollection;
    }

    public Felhasználóicsoport getFelhasználóicsoportID() {
        return felhasználóicsoportID;
    }

    public void setFelhasználóicsoportID(Felhasználóicsoport felhasználóicsoportID) {
        this.felhasználóicsoportID = felhasználóicsoportID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Felhasználó)) {
            return false;
        }
        Felhasználó other = (Felhasználó) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Felhaszn\u00e1l\u00f3[ id=" + id + " ]";
    }

    public int getEgyenleg() {
        return egyenleg;
    }

    public void setEgyenleg(int egyenleg) {
        this.egyenleg = egyenleg;
    }
    
}
