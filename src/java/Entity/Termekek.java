/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gabor_000
 */
@Entity
@Table(name = "Termekek")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Termekek.findAll", query = "SELECT t FROM Termekek t")
    , @NamedQuery(name = "Termekek.findByTermekID", query = "SELECT t FROM Termekek t WHERE t.termekID = :termekID")
    , @NamedQuery(name = "Termekek.findByNev", query = "SELECT t FROM Termekek t WHERE t.nev = :nev")
    , @NamedQuery(name = "Termekek.findByAr", query = "SELECT t FROM Termekek t WHERE t.ar = :ar")
    , @NamedQuery(name = "Termekek.findByKep", query = "SELECT t FROM Termekek t WHERE t.kep = :kep")})
public class Termekek implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "TermekID")
    private Integer termekID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "Nev")
    private String nev;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ar")
    private int ar;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "Kep")
    private String kep;

    public Termekek() {
    }

    public Termekek(Integer termekID) {
        this.termekID = termekID;
    }

    public Termekek(Integer termekID, String nev, int ar, String kep) {
        this.termekID = termekID;
        this.nev = nev;
        this.ar = ar;
        this.kep = kep;
    }

    public Integer getTermekID() {
        return termekID;
    }

    public void setTermekID(Integer termekID) {
        this.termekID = termekID;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public int getAr() {
        return ar;
    }

    public void setAr(int ar) {
        this.ar = ar;
    }

    public String getKep() {
        return kep;
    }

    public void setKep(String kep) {
        this.kep = kep;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (termekID != null ? termekID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Termekek)) {
            return false;
        }
        Termekek other = (Termekek) object;
        if ((this.termekID == null && other.termekID != null) || (this.termekID != null && !this.termekID.equals(other.termekID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Termekek[ termekID=" + termekID + " ]";
    }
    
}
