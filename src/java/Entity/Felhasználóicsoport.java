/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author András
 */
@Entity
@Table(name = "Felhaszn\u00e1l\u00f3i_csoport")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Felhaszn\u00e1l\u00f3icsoport.findAll", query = "SELECT f FROM Felhaszn\u00e1l\u00f3icsoport f")
    , @NamedQuery(name = "Felhaszn\u00e1l\u00f3icsoport.findById", query = "SELECT f FROM Felhaszn\u00e1l\u00f3icsoport f WHERE f.id = :id")
    , @NamedQuery(name = "Felhaszn\u00e1l\u00f3icsoport.findByMegnevez\u00e9s", query = "SELECT f FROM Felhaszn\u00e1l\u00f3icsoport f WHERE f.megnevez\u00e9s = :megnevez\u00e9s")})
public class Felhasználóicsoport implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "Megnevez\u00e9s")
    private String megnevezés;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "felhaszn\u00e1l\u00f3icsoportID")
    private Collection<Felhasználó> felhasználóCollection;

    public Felhasználóicsoport() {
    }

    public Felhasználóicsoport(Integer id) {
        this.id = id;
    }

    public Felhasználóicsoport(Integer id, String megnevezés) {
        this.id = id;
        this.megnevezés = megnevezés;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMegnevezés() {
        return megnevezés;
    }

    public void setMegnevezés(String megnevezés) {
        this.megnevezés = megnevezés;
    }

    @XmlTransient
    public Collection<Felhasználó> getFelhasználóCollection() {
        return felhasználóCollection;
    }

    public void setFelhasználóCollection(Collection<Felhasználó> felhasználóCollection) {
        this.felhasználóCollection = felhasználóCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Felhasználóicsoport)) {
            return false;
        }
        Felhasználóicsoport other = (Felhasználóicsoport) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Felhaszn\u00e1l\u00f3icsoport[ id=" + id + " ]";
    }
    
}
