/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author András
 */
@Entity
@Table(name = "M\u0171t\u00e1rgy")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "M\u0171t\u00e1rgy.findAll", query = "SELECT m FROM M\u0171t\u00e1rgy m")
    , @NamedQuery(name = "M\u0171t\u00e1rgy.findById", query = "SELECT m FROM M\u0171t\u00e1rgy m WHERE m.id = :id")
    , @NamedQuery(name = "M\u0171t\u00e1rgy.findByNeve", query = "SELECT m FROM M\u0171t\u00e1rgy m WHERE m.neve = :neve")
    , @NamedQuery(name = "M\u0171t\u00e1rgy.findByDigit\u00e1lisanyag", query = "SELECT m FROM M\u0171t\u00e1rgy m WHERE m.digit\u00e1lisanyag = :digit\u00e1lisanyag")})
public class Műtárgy implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "Neve")
    private String neve;
    @Lob
    @Size(max = 65535)
    @Column(name = "Le\u00edr\u00e1s")
    private String leírás;
    @Size(max = 500)
    @Column(name = "Digit\u00e1lis_anyag")
    private String digitálisanyag;
    @ManyToMany(mappedBy = "m\u0171t\u00e1rgyCollection")
    private Collection<Kiállítás> kiállításCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "m\u0171t\u00e1rgyID")
    private Collection<Rendelés> rendelésCollection;

    public Műtárgy() {
    }

    public Műtárgy(Integer id) {
        this.id = id;
    }

    public Műtárgy(Integer id, String neve) {
        this.id = id;
        this.neve = neve;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNeve() {
        return neve;
    }

    public void setNeve(String neve) {
        this.neve = neve;
    }

    public String getLeírás() {
        return leírás;
    }

    public void setLeírás(String leírás) {
        this.leírás = leírás;
    }

    public String getDigitálisanyag() {
        return digitálisanyag;
    }

    public void setDigitálisanyag(String digitálisanyag) {
        this.digitálisanyag = digitálisanyag;
    }

    @XmlTransient
    public Collection<Kiállítás> getKiállításCollection() {
        return kiállításCollection;
    }

    public void setKiállításCollection(Collection<Kiállítás> kiállításCollection) {
        this.kiállításCollection = kiállításCollection;
    }

    @XmlTransient
    public Collection<Rendelés> getRendelésCollection() {
        return rendelésCollection;
    }

    public void setRendelésCollection(Collection<Rendelés> rendelésCollection) {
        this.rendelésCollection = rendelésCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Műtárgy)) {
            return false;
        }
        Műtárgy other = (Műtárgy) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.M\u0171t\u00e1rgy[ id=" + id + " ]";
    }
    
}
