/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author András
 */
@Entity
@Table(name = "Rendel\u00e9s")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rendel\u00e9s.findAll", query = "SELECT r FROM Rendel\u00e9s r")
    , @NamedQuery(name = "Rendel\u00e9s.findById", query = "SELECT r FROM Rendel\u00e9s r WHERE r.id = :id")
    , @NamedQuery(name = "Rendel\u00e9s.findById\u0151pont", query = "SELECT r FROM Rendel\u00e9s r WHERE r.id\u0151pont = :id\u0151pont")})
public class Rendelés implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id\u0151pont")
    @Temporal(TemporalType.DATE)
    private Date időpont;
    @JoinColumn(name = "M\u0171t\u00e1rgy_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Műtárgy műtárgyID;
    @JoinColumn(name = "Felhaszn\u00e1l\u00f3_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Felhasználó felhasználóID;

    public Rendelés() {
    }

    public Rendelés(Integer id) {
        this.id = id;
    }

    public Rendelés(Integer id, Date időpont) {
        this.id = id;
        this.időpont = időpont;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getIdőpont() {
        return időpont;
    }

    public void setIdőpont(Date időpont) {
        this.időpont = időpont;
    }

    public Műtárgy getMűtárgyID() {
        return műtárgyID;
    }

    public void setMűtárgyID(Műtárgy műtárgyID) {
        this.műtárgyID = műtárgyID;
    }

    public Felhasználó getFelhasználóID() {
        return felhasználóID;
    }

    public void setFelhasználóID(Felhasználó felhasználóID) {
        this.felhasználóID = felhasználóID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rendelés)) {
            return false;
        }
        Rendelés other = (Rendelés) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Rendel\u00e9s[ id=" + id + " ]";
    }
    
}
